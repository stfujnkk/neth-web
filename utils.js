function checkMetaMask() {
    if (window.ethereum) {
        const { ethereum } = window;
        return ethereum && ethereum.isMetaMask
    }
    return false;
}

function checkEnv() {
    if (!checkMetaMask()) {
        alert('请先安装MetaMask')
        window.location.href = "https://metamask.io/download/"
    }
}

function isAccountAddress(ss) {
    return ethers.utils.isHexString(ss) && ss.length == 42;
}

function isHash(ss) {
    return ethers.utils.isHexString(ss) && ss.length == 66;
}