/**
 * 
 * @param {string} contractName 
 * @param {Array} constructorArgs 
 * @returns {Promise}
 */
async function deploy(contractName, constructorArgs) {
    console.log('Running deployWithEthers script...');
    constructorArgs = constructorArgs || [];
    return axios.get(`/artifacts/${contractName}.json`)
        .then(async resp => {
            const metadata = resp.data;
            // 'provider' is a global variable object
            const signer = provider.getSigner();
            let factory = new ethers.ContractFactory(metadata.abi, metadata.data.bytecode.object, signer);
            let contract = await factory.deploy(...constructorArgs);
            console.log('Contract Address: ', contract.address);
            // The contract is NOT deployed yet; we must wait until it is mined
            let con = await contract.deployed();
            console.log('Deployment successful.');
            return con;
        });
}

/**
 * 
 * @param {string} contractName 
 * @param {string} address 
 * @returns {Promise}
 */
async function findContractAt(contractName, address) {
    return axios.get(`/artifacts/${contractName}.json`)
        .then(async resp => {
            const metadata = resp.data;
            let contract = new ethers.Contract(address, metadata.abi, provider);
            return contract;
        })
}

// let contractWithSigner = contract.connect(signer);

// // 从私钥获取一个签名器 Signer
// let privateKey = '0x0123456789012345678901234567890123456789012345678901234567890123';
// let wallet = new ethers.Wallet(privateKey, provider);
// // 使用签名器创建一个新的合约实例，它允许使用可更新状态的方法
// let contractWithSigner = contract.connect(wallet);
// // ... 或 ...
// // let contractWithSigner = new Contract(contractAddress, abi, wallet)
// // 设置一个新值，返回交易
// let tx = await contractWithSigner.setValue("I like turtles.");
// // 查看: https://ropsten.etherscan.io/tx/0xaf0068dcf728afa5accd02172867627da4e6f946dfb8174a7be31f01b11d5364
// console.log(tx.hash);
// // "0xaf0068dcf728afa5accd02172867627da4e6f946dfb8174a7be31f01b11d5364"
// // 操作还没完成，需要等待挖矿
// await tx.wait();


